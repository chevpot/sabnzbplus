ARG ARCH
FROM linuxserver/sabnzbd:$ARCH

RUN apt-get update && apt-get install -y git python3-pip libxml2-dev libxmlsec1-dev gcc python3-dev python3-setuptools

RUN git clone https://github.com/caronc/nzb-notify.git

RUN cd nzb-notify; pip3 install -r requirements.txt
RUN mkdir /scripts
RUN cp nzb-notify/Notify.py /scripts/
RUN cp nzb-notify/sabnzbd-notify.py /scripts